# MSWizardBundle #

Small Symfony (^3.2) bundle to create form wizards.

## HowTo : ##
after which a normal form was created, the actual steps are only divided into the twig file in step1, step2 ...

**the bundle needs bootsrap and jquery**

## install : ##

### Step 1 ###

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new MS\WizardBundle\MSWizardBundle(),
        // ...
    );
}
```

### Step 2 ###

```php
// routing.yml

ms_wizard:
    resource: "@MSWizardBundle/Resources/config/routing.yml"
    prefix:   /
```

### Step 3 ###

```php
// composer.json

"autoload": {
        "psr-4": {
            "": "src/"
        },
```
	
### Step 4 ###

```php
bin/console assets:install --symlink web
```