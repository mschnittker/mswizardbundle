<?php

namespace MS\WizardBundle\Controller;

use MS\WizardBundle\Entity\Wizard;
use MS\WizardBundle\Form\WizardType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $entity = new Wizard();

        $form = $this->createForm(WizardType::class, $entity, array('method' => 'POST'));
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $results = $this->getDoctrine()
                ->getManager()
                ->getRepository('MS:WizardBundle:Wizard')
                ->getAllResults(null, 'id', 'ASC');

            return $this->redirectToRoute('homepage', array(
                'results' => $results
            ));
        }

        return $this->render('MSWizardBundle:Default:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
