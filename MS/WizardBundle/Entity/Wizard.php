<?php

namespace MS\WizardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Test
 *
 * @ORM\Table(name="wizard")
 * @ORM\Entity(repositoryClass="MS/WizardBundle\Entity\Repository\WizardRepository")
 */
class Wizard
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="field_one", type="text")
     */
    private $field_one;

    /**
     * @var string
     * @ORM\Column(name="field_two", type="text")
     */
    private $field_two;

    /**
     * @var string
     * @ORM\Column(name="field_three", type="text")
     */
    private $field_three;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFieldOne()
    {
        return $this->field_one;
    }

    /**
     * @param string $field_one
     * @return Wizard
     */
    public function setFieldOne($field_one)
    {
        $this->field_one = $field_one;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldTwo()
    {
        return $this->field_two;
    }

    /**
     * @param string $field_two
     * @return Wizard
     */
    public function setFieldTwo($field_two)
    {
        $this->field_two = $field_two;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldThree()
    {
        return $this->field_three;
    }

    /**
     * @param string $field_three
     * @return Wizard
     */
    public function setFieldThree($field_three)
    {
        $this->field_three = $field_three;
        return $this;
    }
}

