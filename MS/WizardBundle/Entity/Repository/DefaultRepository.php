<?php
/**
 * Created by PhpStorm.
 * User: markus
 * Date: 29.09.17
 * Time: 12:24
 */

namespace MS\WizardBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class DefaultRepository extends EntityRepository
{
    /**
     * @param null $limit
     * @param null $orderBy
     * @param string $method
     * @return mixed
     */
    public function getAllResults($limit = null, $orderBy = null, $method = 'DESC')
    {
        $qb = $this->createQueryBuilder('e');

        if(!is_null($limit)){
            $qb->setMaxResults($limit);
        }

        if(!is_null($orderBy)){
            $qb->orderBy('e.'.$orderBy, $method);
        }

        return $qb
            ->getQuery()
            ->execute();
    }

}