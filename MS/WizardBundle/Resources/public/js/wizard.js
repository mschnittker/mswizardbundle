$(document).ready(function(){
    wizard.init();
    wizard.next();
    wizard.prev();
});

var wizard = {
    'progess_bar': $('#progress'),
    'btn_next': $('#btn-next'),
    'btn_prev': $('#btn-prev'),
    'btn_save': $('#btn-save'),
    'items': $('.step-bar > ul > li'),
    'first_tab': $('.step-bar > ul > li').first().data('step'),
    'last_tab': $('.step-bar > ul > li').last().data('step'),
    'getPercent': function (item) {
        var total = wizard.items.length;

        var current = "";
        if(item != ""){
            current = item.data('step');
        } else {
            current = wizard.first_tab;
        }

        return percent = (current / total) * 100;
    },
    'showCurrentTab': function (current) {
        for(var i = wizard.first_tab; i <= wizard.items.length; i++){
            if(i != current){
                $('#step-' + i).addClass('hide');
            } else {
                $('#step-' + i).removeClass('hide');
            }
        }
    },
    'init': function () {
        // init progressbar with start value
        $(wizard.progess_bar).css('width', wizard.getPercent("") + '%');

        // hide all unused buttons
        wizard.btn_prev.addClass('hide');
        wizard.btn_save.addClass('hide');

        // hide all unused tabs
        var current = wizard.items.first().data('step');
        for(var i = ++current; i <= wizard.items.length; i++){
            $('#step-' + i).addClass('hide');
        }
    },
    'next': function () {
        $(wizard.btn_next).click(function () {
            var next = wizard.items.filter('.active').removeClass('active').next();
            next.addClass('active');

            var current = next.data('step');
            wizard.showCurrentTab(current);

            if(current == wizard.last_tab){
                wizard.btn_next.addClass('hide');
                wizard.btn_save.removeClass('hide');
            } else {
                wizard.btn_next.removeClass('hide');
                wizard.btn_save.addClass('hide');
            }

            if(current != wizard.first_tab){
                wizard.btn_prev.removeClass('hide');
            }

            $(wizard.progess_bar).css('width', wizard.getPercent(next) + '%');
        });
    },
    'prev': function () {
        $(wizard.btn_prev).click(function () {
            var prev = wizard.items.filter('.active').removeClass('active').prev();
            prev.addClass('active');

            var current = prev.data('step');
            wizard.showCurrentTab(current);

            if(current == wizard.first_tab){
                wizard.btn_prev.toggleClass('hide');
            } else {
                wizard.btn_prev.removeClass('hide');
            }

            if(current != wizard.last_tab){
                wizard.btn_next.removeClass('hide');
                wizard.btn_save.addClass('hide');
            }

            $(wizard.progess_bar).css('width', wizard.getPercent(prev) + '%');
        });
    }
};
