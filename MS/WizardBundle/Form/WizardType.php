<?php

namespace MS\WizardBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class WizardType extends AbstractType
{
    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * TestType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translator = $this->container->get('translator');

        $builder
            ->add('field_one', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => $translator->trans('form.input', array(), 'forms'),
                    'class' => 'form-control'
                )
            ))
            ->add('field_two', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => $translator->trans('form.input', array(), 'forms'),
                    'class' => 'form-control'
                )
            ))
            ->add('field_three', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => $translator->trans('form.input', array(), 'forms'),
                    'class' => 'form-control'
                )
            ))
            ;
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MS\WizardBundle\Entity\Wizard'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_test';
    }


}
